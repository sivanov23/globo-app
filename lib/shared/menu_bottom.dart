// ignore_for_file: prefer_const_literals_to_create_immutables

import "package:flutter/material.dart";

class MenuBottom extends StatelessWidget {
  const MenuBottom({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      onTap: (int index) {
        switch (index) {
          case 0: 
            Navigator.pushNamed(context, '/');
            break;
          case 1:
            Navigator.pushNamed(context, '/bmi');
            break; 
          case 2: 
            Navigator.pushNamed(context, '/weather');  
        } 
      },
      items: [
        const BottomNavigationBarItem(
          icon:  Icon(Icons.home),
          label: 'Home'
        ),
        const BottomNavigationBarItem(
          icon: Icon(Icons.monitor_weight),
          label: 'BMI'
        ),
        const BottomNavigationBarItem(
          icon: Icon(Icons.sunny_snowing),
          label: 'Weather'
        ),
           
      ],
    );
  }
}