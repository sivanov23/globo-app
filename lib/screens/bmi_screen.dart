// ignore_for_file: sort_child_properties_last

import "package:flutter/material.dart";
import "package:globo_fitness/shared/menu_bottom.dart";
import "package:globo_fitness/shared/menu_drawer.dart";

class BmiScreen extends StatefulWidget {
  const BmiScreen({super.key});

  @override
  State<BmiScreen> createState() => _BmiScreenState();
}

class _BmiScreenState extends State<BmiScreen> {
  
  final TextEditingController txtHeight = TextEditingController();
  final TextEditingController txtWeight = TextEditingController();

  late List<bool> isSelected;
  
  final double fontSizeConstant = 18;
  String result = '';
  bool isMetric = true;
  bool isImperial = false;
  double? height = 0;
  double? weight = 0;
  String heightMessage = '';
  String weightMessage = '';

  @override
  void initState() {
    isSelected = [isMetric, isImperial];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    heightMessage = 'Please insert your weight in ${(isMetric) ? 'meters' : 'inches'}';
    weightMessage = 'Please insert your height in ${(isMetric) ? 'kilos' : 'pounds'}';
    return Scaffold(
        appBar: AppBar(title: const Text('BMI Calculator')),
        drawer: const MenuDrawer(),
        bottomNavigationBar: const MenuBottom(),
        body: SingleChildScrollView(
          child: Column(
            children: [
              ToggleButtons(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16),
                    child: Text('Metric', style: TextStyle(fontSize: fontSizeConstant))
                  ) ,
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16),
                    child: Text('Imperial', style: TextStyle(fontSize: fontSizeConstant))
                  ),
                ],
                isSelected: isSelected,
                onPressed: toggleMeasure , 
              ),
              Padding(
                padding: const EdgeInsets.all(32.0),
                child: TextField(
                  controller: txtHeight,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    hintText: heightMessage,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(32.0),
                child: TextField(
                  controller: txtWeight,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    hintText: weightMessage,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(32.0),
                child: ElevatedButton(
                  child: Text(
                    'Calculate BMI',
                    style: TextStyle( fontSize: fontSizeConstant)),
                    onPressed: findBMI,
                ),
              ),
              Text(
              result,
              style: TextStyle( 
                fontSize: fontSizeConstant
                )),
            ],
          ),
        )
    );
  }

  void toggleMeasure(value) {
    if (value == 0) {
      isMetric = true;
      isImperial = false; 
    } else {
      isMetric = false; 
      isImperial = true;
    }
    setState(() {
      isSelected = [isMetric, isImperial];
    });
  }

  void findBMI() {
    double bmi = 1;
    double height = double.tryParse(txtHeight.text) ?? 1;
    double weight = double.tryParse(txtWeight.text) ?? 1;
    if (isMetric) {
      bmi = weight / (height * height);
    } else {
      bmi = weight * 703 / (height * height);
    } setState(() {
        result = 'Your BMI is ${bmi.toStringAsFixed(2)}';
    });
  }
}
