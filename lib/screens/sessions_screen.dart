import "package:flutter/material.dart";
import "package:flutter/widgets.dart";
import "package:globo_fitness/data/session.dart";
import "package:globo_fitness/data/shared_preferences_helper.dart";
import "package:globo_fitness/shared/menu_bottom.dart";
import "package:globo_fitness/shared/menu_drawer.dart";

class SessionsScreen extends StatefulWidget {
  const SessionsScreen({super.key});

  @override
  State<SessionsScreen> createState() => __SessionsScreenState();
}

class __SessionsScreenState extends State<SessionsScreen> {
  
  List<Session> sessions = [];
  Map<int, String> months = {
    1: 'January',
    2: 'February',
    3: 'March',
    4: 'April',
    5: 'May',
    6: 'June',
    7: 'July',
    8: 'August',
    9: 'September',
    10: 'Octomber',
    11: 'November',
    12: 'December',
  };

  final TextEditingController txtDescription = TextEditingController();
  final TextEditingController txtDuration = TextEditingController();
  final SharedPreferencesHelper helper = SharedPreferencesHelper();

  @override
  void initState() {
    helper.init().then((value) {
      updateScreen();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Your training sessions')),
      drawer: const MenuDrawer(),
      bottomNavigationBar: const MenuBottom(),
      body: ListView(children: getContent()),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.add),
        onPressed: () {
          showSessionDialog(context);
        },
      ),
      // bottomNavigationBar: const MenuDrawer(),
    );
  }

  Future<dynamic> showSessionDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('Insert Training Sess'),
            content: SingleChildScrollView(
                child: Column(children: [
              TextField(
                controller: txtDescription,
                decoration: const InputDecoration(hintText: 'Description'),
              ),
              TextField(
                controller: txtDuration,
                decoration: const InputDecoration(hintText: 'Duration'),
              )
            ])),
            actions: [
              TextButton(
                  child: const Text('Cancel'),
                  onPressed: () {
                    Navigator.pop(context);
                    txtDescription.text = '';
                    txtDuration.text = '';
                  }),
              ElevatedButton(onPressed: saveSession, child: const Text('Save'))
            ],
          );
        });
  }

  Future saveSession() async {
    DateTime now = DateTime.now();
    String today = '${now.day}-${months[now.month]}-${now.year}';
    int sessionId = helper.getCounter() + 1; 
    Session newSession = Session(
        sessionId, today, txtDescription.text, int.tryParse(txtDuration.text) ?? 0);
    helper.writeSession(newSession).then((_){
      updateScreen();
      helper.setCounter(); 
    });
    txtDescription.text = '';
    txtDuration.text = '';
    Navigator.pop(context);
  }

  List<Widget> getContent() {
    List<Widget> tiles = [];
    // ignore: avoid_function_literals_in_foreach_calls
    sessions.forEach((session) {
      tiles.add(Dismissible(
        key: UniqueKey(),
        onDismissed: (_) {
          helper.deleteSession(session.id).then((value) => updateScreen());
        },
        child: ListTile(
          title: Text(session.description),
          subtitle: Text('${session.date}-${session.duration}-min'),
        ),
      ));
    });
    return tiles;
  }

  void updateScreen() {
    sessions = helper.getSessions();
    setState(() {});
  }
}
